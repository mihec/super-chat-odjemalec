$(document).ready(function() {
    var baseUrl = "https://chat.data-lab.si/api";
    var user = {id: 20, name: "Miha Piha"}; //TODO: vnesi svoje podatke
    var nextMessageId = 0;
    var currentRoom = "Skedenj";

    //Naloži seznam sob
    $.ajax({
        url: baseUrl + "/rooms",
        type: "GET",
        success: function (data) {
            for (i in data) {
                $("#rooms").append(" \
                    <li class='media'> \
                        <div class='media-body room' style='cursor: pointer;'> \
                            <div class='media'> \
                                <a class='pull-left' href='#'> \
                                    <img class='media-object img-circle' src='img/" + data[i] + ".jpg' /> \
                                </a> \
                                <div class='media-body'> \
                                    <h5>" + data[i] + "</h5> \
                                </div> \
                            </div> \
                        </div> \
                    </li>");
            }
            $(".media-body").click(menjavaSobe);
        }
    });

    //TODO: Naloga
    //Definicija funkcije za pridobivanje pogovorov, ki se samodejno ponavlja na 5 sekund
    var osvezevanjePogovorov = function(){
        $.ajax({
            url: baseUrl + "/messages/" + currentRoom + "/" + nextMessageId,
            type: "GET",
            success: function(data){
                for(i in data){
                    $("#messages").append('\
                    <li class="media">\
                            <div class="media-body">\
                                <div class="media">\
                                    <a class="pull-left" href="#">\
                                        <img class="media-object img-circle " src="https://randomuser.me/api/portraits/men/'+data[i].user.id+'.jpg" />\
                                    </a>\
                                    <div class="media-body" >\
                                        <small class="text-muted">'+data[i].user.name+' | '+data[i].time+'</small> <br />\
                                        '+data[i].text+'\
                                        <hr />\
                                    </div>\
                                </div>\
                            </div>\
                        </li>\
                    ');
                    
                    nextMessageId = data[i].id + 1;
                }
            }
        });
    }
    //Klicanje funkcije za začetek nalaganja pogovorov
    $("#messages").html("");
    window.setInterval(function(){
        osvezevanjePogovorov();
        osvezevanjeUporabnikov();
    }, 5000);
    
    //TODO: Naloga
    //Definicija funkcije za posodabljanje seznama uporabnikov, ki se samodejno ponavlja na 5 sekund
    var osvezevanjeUporabnikov = function(){
        $("#users").html("");
        $.ajax({
            url: baseUrl + "/users/" + currentRoom,
            type: "GET",
            success: function(data){
                for (i in data){
                    $("#users").append('<li class="media">\
                    <div class="media-body">\
                        <div class="media">\
                                    <a class="pull-left" href="#">\
                                        <img class="media-object img-circle" src="https://randomuser.me/api/portraits/men/'+data[i].id+'.jpg" />\
                                    </a>\
                                    <div class="media-body" >\
                                        <h5>'+data[i].name+' </h5>\
                                    </div>\
                                </div>\
                            </div>\
                        </li>');
                }
            }
        });
    }
    //Klicanje funkcije za začetek posodabljanja uporabnikov

    //TODO: Naloga
    //Definicija funkcije za pošiljanje sporočila
    var posljiSporocilo = function(){
        $.ajax({
            url: baseUrl + "/messages/" + currentRoom,
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify({user: user, text: $("#message").val()}),
            success: function(data){
                $("#message").val("");
            },
            error: function(err){
                alert("Prišlo je do napake pri pošiljanju sporočila. Prosimo poskusite znova!");
            }
        });
    }
    //On Click handler za pošiljanje sporočila
    $("#send").click(posljiSporocilo);

    //TODO: Naloga
    //Definicija funkcije za menjavo sobe (izbriši pogovore in uporabnike na strani, nastavi spremenljivko currentRoom, nastavi spremenljivko nextMessageId na 0)
    //V razmislek: pomisli o morebitnih težavah!
    var menjavaSobe = function(event){
        currentRoom = event.currentTarget.getElementsByTagName("h5")[0].innerHTML;
        nextMessageId = 0;
        $("#messages").html("");
        $("#users").html("");
    }
    //On Click handler za menjavo sobe
    //Namig: Seznam sob se lahko naloži kasneje, kot koda, ki se izvede tu.
    
});